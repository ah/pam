Description: rebuild README files with current docs toolchain
 This incorporates certain insignificant changes to whitespace handling
 in the toolchain for documentation generation, which allows us to have a
 clean ./debian/rules build; ./debian/rules clean round-trip.
Author: Steve Langasek <vorlon@debian.org>
Last-Updated: 2018-01-08
Forwarded: not-needed

diff --git a/modules/pam_access/README b/modules/pam_access/README
index 3ab46871..8ee1b2f4 100644
--- a/modules/pam_access/README
+++ b/modules/pam_access/README
@@ -83,7 +83,7 @@ instead. The same meaning of 192.168.201. is 192.168.201.0/24 or 192.168.201.0/
 
 + : root : 192.168.201.
 
-User root should be able to have access from hosts foo1.bar.org and
+User root should be able to have access from hosts foo1.bar.org and 
 foo2.bar.org (uses string matching also).
 
 + : root : foo1.bar.org foo2.bar.org
diff --git a/modules/pam_filter/README b/modules/pam_filter/README
index 4d4e2194..2978e546 100644
--- a/modules/pam_filter/README
+++ b/modules/pam_filter/README
@@ -45,17 +45,17 @@ runX
     have read the pam(3) manual page. Basically, for each management group
     there are up to two ways of calling the module's functions. In the case of
     the authentication and session components there are actually two separate
-    functions. For the case of authentication, these functions are
+    functions. For the case of authentication, these functions are 
     pam_authenticate(3) and pam_setcred(3), here run1 means run the filter from
     the pam_authenticate function and run2 means run the filter from
     pam_setcred. In the case of the session modules, run1 implies that the
-    filter is invoked at the pam_open_session(3) stage, and run2 for
+    filter is invoked at the pam_open_session(3) stage, and run2 for 
     pam_close_session(3).
 
     For the case of the account component. Either run1 or run2 may be used.
 
     For the case of the password component, run1 is used to indicate that the
-    filter is run on the first occasion of pam_chauthtok(3) (the
+    filter is run on the first occasion of pam_chauthtok(3) (the 
     PAM_PRELIM_CHECK phase) and run2 is used to indicate that the filter is run
     on the second occasion (the PAM_UPDATE_AUTHTOK phase).
 
diff --git a/modules/pam_ftp/README b/modules/pam_ftp/README
index 15f4130e..b9ef7857 100644
--- a/modules/pam_ftp/README
+++ b/modules/pam_ftp/README
@@ -7,7 +7,7 @@ DESCRIPTION
 pam_ftp is a PAM module which provides a pluggable anonymous ftp mode of
 access.
 
-This module intercepts the user's name and password. If the name is ftp or
+This module intercepts the user's name and password. If the name is ftp or 
 anonymous, the user's password is broken up at the @ delimiter into a PAM_RUSER
 and a PAM_RHOST part; these pam-items being set accordingly. The username (
 PAM_USER) is set to ftp. In this case the module succeeds. Alternatively, the
diff --git a/modules/pam_listfile/README b/modules/pam_listfile/README
index e1aaf8cc..5f926bdf 100644
--- a/modules/pam_listfile/README
+++ b/modules/pam_listfile/README
@@ -7,7 +7,7 @@ DESCRIPTION
 pam_listfile is a PAM module which provides a way to deny or allow services
 based on an arbitrary file.
 
-The module gets the item of the type specified -- user specifies the username,
+The module gets the item of the type specified -- user specifies the username, 
 PAM_USER; tty specifies the name of the terminal over which the request has
 been made, PAM_TTY; rhost specifies the name of the remote host (if any) from
 which the request was made, PAM_RHOST; and ruser specifies the name of the
@@ -24,7 +24,7 @@ appropriate) will be returned.
 
 An additional argument, apply=, can be used to restrict the application of the
 above to a specific user (apply=username) or a given group (apply=@groupname).
-This added restriction is only meaningful when used with the tty, rhost and
+This added restriction is only meaningful when used with the tty, rhost and 
 shell items.
 
 Besides this last one, all arguments should be specified; do not count on any
diff --git a/modules/pam_mail/README b/modules/pam_mail/README
index a0a0b7d9..8fe8721c 100644
--- a/modules/pam_mail/README
+++ b/modules/pam_mail/README
@@ -45,7 +45,7 @@ noenv
 
 nopen
 
-    Don't print any mail information on login. This flag is useful to get the
+    Don't print any mail information on login. This flag is useful to get the 
     MAIL environment variable set, but to not display any information about it.
 
 quiet
diff --git a/modules/pam_namespace/README b/modules/pam_namespace/README
index 41cc5403..6c580d6a 100644
--- a/modules/pam_namespace/README
+++ b/modules/pam_namespace/README
@@ -173,7 +173,7 @@ the tmpfs instance that is created by the mount call. See mount(8) for details.
 
 The directory where polyinstantiated instances are to be created, must exist
 and must have, by default, the mode of 0000. The requirement that the instance
-parent be of mode 0000 can be overridden with the command line option
+parent be of mode 0000 can be overridden with the command line option 
 ignore_instance_parent_mode
 
 In case of context or level polyinstantiation the SELinux context which is used
diff --git a/modules/pam_rhosts/README b/modules/pam_rhosts/README
index b1911785..aedc0f5d 100644
--- a/modules/pam_rhosts/README
+++ b/modules/pam_rhosts/README
@@ -17,7 +17,7 @@ identical to their local one, or if their remote account has an entry in their
 personal configuration file.
 
 The module authenticates a remote user (internally specified by the item
-PAM_RUSER connecting from the remote host (internally specified by the item
+PAM_RUSER connecting from the remote host (internally specified by the item 
 PAM_RHOST). Accordingly, for applications to be compatible this authentication
 module they must set these items prior to calling pam_authenticate(). The
 module is not capable of independently probing the network connection for such
diff --git a/modules/pam_tally/README b/modules/pam_tally/README
index 06e8f092..85aa1607 100644
--- a/modules/pam_tally/README
+++ b/modules/pam_tally/README
@@ -32,7 +32,7 @@ GLOBAL OPTIONS
 
     onerr=[fail|succeed]
 
-        If something weird happens (like unable to open the file), return with
+        If something weird happens (like unable to open the file), return with 
         PAM_SUCCESS if onerr=succeed is given, else with the corresponding PAM
         error code.
 
diff --git a/modules/pam_userdb/README b/modules/pam_userdb/README
index 8e1a5ffd..0c256a90 100644
--- a/modules/pam_userdb/README
+++ b/modules/pam_userdb/README
@@ -13,7 +13,7 @@ OPTIONS
 crypt=[crypt|none]
 
     Indicates whether encrypted or plaintext passwords are stored in the
-    database. If it is crypt, passwords should be stored in the database in
+    database. If it is crypt, passwords should be stored in the database in 
     crypt(3) form. If none is selected, passwords should be stored in the
     database as plaintext.
 
